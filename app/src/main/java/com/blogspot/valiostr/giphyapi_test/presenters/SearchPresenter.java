package com.blogspot.valiostr.giphyapi_test.presenters;

/**
 * Created by Valiostr on 27.04.2017.
 */

interface SearchPresenter {

    void bind(SearchView view);

    void unBind();

    public void search(String pWord);

    public void getNextPage(int offset, int pageSize);
}
