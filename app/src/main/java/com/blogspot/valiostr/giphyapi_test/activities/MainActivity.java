package com.blogspot.valiostr.giphyapi_test.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;

import com.blogspot.valiostr.giphyapi_test.R;
import com.blogspot.valiostr.giphyapi_test.adapters.GifAdapter;
import com.blogspot.valiostr.giphyapi_test.models.Search;
import com.blogspot.valiostr.giphyapi_test.presenters.SearchPresenterImpl;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements com.blogspot.valiostr.giphyapi_test.presenters.SearchView {

    @BindView(R.id.gifList)
    RecyclerView mGifsListRecyclerView;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    private SearchView mSearchView;
    private GifAdapter mGifAdapter;
    private SearchPresenterImpl mSearchPresenter = new SearchPresenterImpl();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mGifAdapter = new GifAdapter(mSearchPresenter);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);

        mGifsListRecyclerView.setLayoutManager(layoutManager);
        mGifsListRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        mGifsListRecyclerView.setAdapter(mGifAdapter);
        mGifsListRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mGifsListRecyclerView.setHasFixedSize(true);
    }

    public void changeContent(){

    }

    @Override
    protected void onResume() {
        super.onResume();
        mSearchPresenter.bind(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSearchPresenter.unBind();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search, menu);

        mSearchView = (SearchView) menu.findItem(R.id.app_bar_search).getActionView();
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.e("Submit", "Submit");
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (!TextUtils.isEmpty(newText)) {
                    mSearchPresenter.search(newText);
                }
                Log.e("Change", newText);
                return false;
            }
        });
        return  true;
    }

    @Override
    public void searchDone(Search search) {
        mGifAdapter.setSearch(search);
    }

    @Override
    public void nextPageDone(Search search) {
        mGifAdapter.addNextPage(search);
    }

    @Override
    public void onDetailsOpen(String url) {
        DetailsAbility.launchActivity(this, url);
    }

    @Override
    public void onErrorDone() {

    }
}
