package com.blogspot.valiostr.giphyapi_test.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.blogspot.valiostr.giphyapi_test.R;
import com.blogspot.valiostr.giphyapi_test.adapters.wrappers.GifWrapper;
import com.blogspot.valiostr.giphyapi_test.models.Datum;
import com.blogspot.valiostr.giphyapi_test.models.Search;
import com.blogspot.valiostr.giphyapi_test.presenters.SearchPresenterImpl;

/**
 * Created by Valiostr on 17.04.2017.
 */

public class GifAdapter extends RecyclerView.Adapter<GifWrapper> {

    private Search mSearch;
    private SearchPresenterImpl mSearchPresenter;

    public GifAdapter(SearchPresenterImpl searchPresenter) {
        mSearchPresenter = searchPresenter;
    }

    @Override
    public GifWrapper onCreateViewHolder(ViewGroup parent, int viewType) {
        return new GifWrapper(LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.adapter_gif_item, parent, false),
                mSearchPresenter);
    }

    @Override
    public void onBindViewHolder(GifWrapper holder, int position) {
        holder.bindItem(getItemByPosition(position), position);
    }

    @Override
    public void onViewAttachedToWindow(GifWrapper holder) {
        super.onViewAttachedToWindow(holder);
        if(holder.getPositionID() == getItemCount()-1){
            mSearchPresenter.getNextPage(getItemCount(), 20);
        }
    }

    private Datum getItemByPosition(int position){
        if(mSearch != null) {
            return mSearch.getData().get(position);
        }else{
            return null;
        }
    }

    public void setSearch(Search search) {
        mSearch = search;
        notifyDataSetChanged();
    }

    public void addNextPage(Search search){
        if (mSearch != null) {
            mSearch.addData(search.getData());
            notifyDataSetChanged();
        }
    }

    @Override
    public int getItemCount() {
        if(mSearch != null) {
            return mSearch.getData().size();
        }else{
            return 5;
        }
    }
}
