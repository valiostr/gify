package com.blogspot.valiostr.giphyapi_test.views;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

public class GifView extends AppCompatImageView {

    private String mUrl = "https://media1.giphy.com/media/G3773sSDJHHy0/giphy.gif";

    public GifView(Context context) {
        super(context);
        init(null, 0);
    }

    public GifView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public GifView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    public void loadUrl(String url){
        if(url != null) {
            mUrl = url;
            updateGif();
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        updateGif();
    }

    private void updateGif(){
        Glide.clear(this);
        Glide.with(getContext())
                .load(mUrl)
                .asGif()
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .fitCenter()
                .into(this);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        Glide.clear(this);
    }

    private void init(AttributeSet attrs, int defStyle) {

    }
}
