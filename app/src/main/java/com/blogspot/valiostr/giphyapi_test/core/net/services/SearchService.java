package com.blogspot.valiostr.giphyapi_test.core.net.services;

import com.blogspot.valiostr.giphyapi_test.models.Search;

import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Valiostr on 10.04.2017.
 */

public interface SearchService {

    @GET("/v1/gifs/search")
    io.reactivex.Observable<Search> getSearchData(@Query("q")String query, @Query("limit")int limit
            , @Query("offset")int offset, @Query("api_key") String apiKey);
}
