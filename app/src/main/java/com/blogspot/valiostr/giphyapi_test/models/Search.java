
package com.blogspot.valiostr.giphyapi_test.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Search implements Parcelable
{

    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("pagination")
    @Expose
    private Pagination pagination;
    @SerializedName("meta")
    @Expose
    private Meta meta;
    public final static Parcelable.Creator<Search> CREATOR = new Creator<Search>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Search createFromParcel(Parcel in) {
            Search instance = new Search();
            in.readList(instance.data, (Datum.class.getClassLoader()));
            instance.pagination = ((Pagination) in.readValue((Pagination.class.getClassLoader())));
            instance.meta = ((Meta) in.readValue((Meta.class.getClassLoader())));
            return instance;
        }

        public Search[] newArray(int size) {
            return (new Search[size]);
        }

    }
    ;

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public void addData(List<Datum> data){
        this.data.addAll(data);
    }

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(data);
        dest.writeValue(pagination);
        dest.writeValue(meta);
    }

    public int describeContents() {
        return  0;
    }

}
