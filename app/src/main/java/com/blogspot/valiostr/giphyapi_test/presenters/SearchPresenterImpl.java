package com.blogspot.valiostr.giphyapi_test.presenters;

import android.os.Handler;
import android.os.Looper;

import com.blogspot.valiostr.giphyapi_test.core.net.managers.SearchManager;
import com.blogspot.valiostr.giphyapi_test.models.Search;

/**
 * Created by Valiostr on 27.04.2017.
 */

public class SearchPresenterImpl extends BasePresenter<SearchView> implements SearchPresenter, ClickPresenter {

    private SearchManager mSearchManager = new SearchManager();
    private String mWord;

    @Override
    public void bind(SearchView view) {
        bindView(view);
    }

    @Override
    public void unBind() {
        unBindView();
    }

    @Override
    public void search(String pWord) {
        mWord = pWord;
        mSearchManager.postDelayedSearch(pWord, new SearchManager.SearchListener() {
            @Override
            public void onSearchDone(final Search search) {
                if (isViewAdded()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            getView().searchDone(search);
                        }
                    });
                }
            }
        });
    }

    @Override
    public void getNextPage(int offset, int pageSize) {
        mSearchManager.pageSearch(mWord, offset, pageSize, new SearchManager.SearchListener() {
            @Override
            public void onSearchDone(final Search search) {
                if (isViewAdded()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            getView().nextPageDone(search);
                        }
                    });
                }
            }
        });
    }

    @Override
    public void onClickGif(String url) {
        if (isViewAdded()) {
            getView().onDetailsOpen(url);
        }
    }
}
