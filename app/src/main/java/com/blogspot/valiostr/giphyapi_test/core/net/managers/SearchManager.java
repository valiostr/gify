package com.blogspot.valiostr.giphyapi_test.core.net.managers;

import android.util.Log;

import com.blogspot.valiostr.giphyapi_test.core.net.NetworkManager;
import com.blogspot.valiostr.giphyapi_test.core.net.services.SearchService;
import com.blogspot.valiostr.giphyapi_test.models.Search;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Valiostr on 10.04.2017.
 */

public class SearchManager {
    private SearchService mSearchService;
    private Observable<Search> mSearchObservable;

    public void searchSomething() {
        getSearchService()
                .getSearchData("cat", 2, 0, "dc6zaTOxFJmzC")
                .subscribeOn(Schedulers.trampoline())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Search>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Search value) {
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    public void postDelayedSearch(String pWord, final SearchListener searchListener) {

        if (mSearchObservable != null) {
            mSearchObservable.unsubscribeOn(Schedulers.newThread());
            mSearchObservable = null;
        }
        mSearchObservable = getSearchService()
                .getSearchData(pWord, 5, 0, "dc6zaTOxFJmzC");
        mSearchObservable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .delay(200, TimeUnit.MILLISECONDS)
                .subscribe(new Observer<Search>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.e("TAG", "TAG");
                    }

                    @Override
                    public void onNext(Search value) {
                        searchListener.onSearchDone(value);
                        Log.e("TAG", "TAG");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("TAG", "TAG");
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    public void pageSearch(String pWord, int offset, int pageSize, final SearchListener searchListener) {
        if (mSearchObservable != null) {
            mSearchObservable.unsubscribeOn(Schedulers.newThread());
            mSearchObservable = null;
        }
        mSearchObservable = getSearchService()
                .getSearchData(pWord, pageSize, offset, "dc6zaTOxFJmzC");
        mSearchObservable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Search>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(Search value) {
                        searchListener.onSearchDone(value);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    private SearchService getSearchService() {
        if (mSearchService == null) {
            mSearchService = NetworkManager.getInstance().getSearchService();
        }
        return mSearchService;
    }

    public interface SearchListener {
        public void onSearchDone(Search search);
    }
}
