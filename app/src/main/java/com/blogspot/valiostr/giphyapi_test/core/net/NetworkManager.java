package com.blogspot.valiostr.giphyapi_test.core.net;

import com.blogspot.valiostr.giphyapi_test.core.net.services.SearchService;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Valiostr on 10.04.2017.
 */

public class NetworkManager {

    private static NetworkManager INSTANCE;
    private Retrofit mRetrofit;
    private SearchService mSearchService;

    private NetworkManager(){

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        mRetrofit = new Retrofit.Builder()
                .baseUrl("http://api.giphy.com")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        mSearchService = mRetrofit.create(SearchService.class);
    }

    public static NetworkManager getInstance() {
        if(INSTANCE == null){
            INSTANCE = new NetworkManager();
        }
        return INSTANCE;
    }

    public SearchService getSearchService() {
        return mSearchService;
    }
}
