package com.blogspot.valiostr.giphyapi_test.presenters;

import com.blogspot.valiostr.giphyapi_test.models.Search;

/**
 * Created by Valiostr on 27.04.2017.
 */

public interface SearchView extends BaseView {

    public void searchDone(Search search);

    public void nextPageDone(Search search);

    public void onDetailsOpen(String url);

    public void onErrorDone();
}
