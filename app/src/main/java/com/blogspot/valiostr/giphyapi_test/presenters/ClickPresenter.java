package com.blogspot.valiostr.giphyapi_test.presenters;

/**
 * Created by Valiostr on 30.04.2017.
 */

public interface ClickPresenter {

    public void onClickGif(String url);
}
