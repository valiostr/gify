package com.blogspot.valiostr.giphyapi_test.adapters.wrappers;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.blogspot.valiostr.giphyapi_test.R;
import com.blogspot.valiostr.giphyapi_test.models.Datum;
import com.blogspot.valiostr.giphyapi_test.presenters.SearchPresenterImpl;
import com.blogspot.valiostr.giphyapi_test.views.GifView;

/**
 * Created by Valiostr on 17.04.2017.
 */

public class GifWrapper extends RecyclerView.ViewHolder {

    private GifView mGifView;
    private int mPosition = -1;
    private SearchPresenterImpl mSearchPresenter;
    private Datum mDatum;

    public GifWrapper(View itemView, final SearchPresenterImpl presenter) {
        super(itemView);
        mGifView = (GifView)itemView.findViewById(R.id.gifView);
        mSearchPresenter = presenter;
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mDatum != null){
                    presenter.onClickGif(mDatum.getImages().getOriginal().getUrl());
                }
            }
        });
    }

    public void bindItem(Datum datum, int position) {
        mPosition = position;
        if(datum != null) {
            mDatum = datum;
            mGifView.loadUrl(datum.getImages().getPreviewGif().getUrl());
        }
    }

    public int getPositionID() {
        return mPosition;
    }
}
