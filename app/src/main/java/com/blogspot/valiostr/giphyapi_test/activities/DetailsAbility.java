package com.blogspot.valiostr.giphyapi_test.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.blogspot.valiostr.giphyapi_test.R;
import com.blogspot.valiostr.giphyapi_test.views.GifView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Valiostr on 30.04.2017.
 */

public class DetailsAbility extends AppCompatActivity {

    private static final String URL = "URL";

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.gifView)
    GifView mGifView;


    public static void launchActivity(Context context, String url){
        Intent intent = new Intent(context, DetailsAbility.class);
        intent.putExtra(URL, url);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_details);

        ButterKnife.bind(this);

        mGifView.loadUrl(getIntent().getStringExtra(URL));

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

    }
}
