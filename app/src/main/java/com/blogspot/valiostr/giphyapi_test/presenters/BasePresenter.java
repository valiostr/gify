package com.blogspot.valiostr.giphyapi_test.presenters;

import java.lang.ref.WeakReference;

/**
 * Created by Valiostr on 27.04.2017.
 */

public abstract class BasePresenter<T extends BaseView> {
    private WeakReference<T> mView;

    protected T getView() {
        return this.mView.get();
    }

    protected void setView(T view) {
        this.mView = new WeakReference<>(view);
    }

    protected void bindView(T view) {
        setView(view);
    }

    protected void unBindView() {
        clearView();
    }

    private void clearView() {
        if (this.mView != null && this.mView.get() != null) {
            this.mView.clear();
            this.mView = null;
        }
    }

    protected boolean isViewAdded() {
        return mView != null && mView.get() != null;
    }
}