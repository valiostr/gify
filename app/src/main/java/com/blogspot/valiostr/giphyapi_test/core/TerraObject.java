package com.blogspot.valiostr.giphyapi_test.core;

import android.app.Application;

import com.squareup.leakcanary.LeakCanary;

/**
 * Created by Valiostr on 10.04.2017.
 */

public class TerraObject extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        LeakCanary.install(this);
    }
}
